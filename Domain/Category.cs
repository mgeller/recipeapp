﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Category
    {
        public int CategoryId { get; set; }

        [MaxLength(128)]
        [Required] public string CategoryName { get; set; } = default!;
    }
}