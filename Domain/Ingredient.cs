﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Ingredient
    {
        public int IngredientId { get; set; }

        [MaxLength(128)]
        [Required]public string IngredientName { get; set; } = default!;

        public int LocationId { get; set; }
        public Location? Location { get; set; }

        public int Amount { get; set; }
    }
}