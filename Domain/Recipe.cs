﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Recipe
    {
        public int RecipeId { get; set; }

        [MaxLength(128)]
        [Required] public string RecipeName { get; set; } = default!;

        public int TimeNeeded { get; set; }

        public ICollection<IngredientRecipe>? IngredientRecipes { get; set; }

        public int Servings { get; set; }
        

        public int CategoryId { get; set; }

        public Category? Category { get; set; }
        
        
        [MaxLength(1024)]
        [Required]public string Instruction { get; set; } = default!;
    }
}