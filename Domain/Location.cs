﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Location
    {
        public int LocationId { get; set; }

        [MaxLength(128)]
        [Required] public string LocationName { get; set; } = default!;
    }
}