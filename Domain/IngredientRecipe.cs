﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class IngredientRecipe
    {
        public int IngredientRecipeId { get; set; }
        

        public int IngredientAmount { get; set; }
        

        public int RecipeId { get; set; }

        public Recipe? Recipe { get; set; }

        
        public int IngredientId { get; set; }

        public Ingredient? Ingredient { get; set; }
    }
}