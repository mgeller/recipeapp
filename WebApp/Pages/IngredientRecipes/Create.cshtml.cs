using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages_IngredientRecipes
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["IngredientId"] = new SelectList(_context.Ingredients, "IngredientId", "IngredientName");
        ViewData["RecipeId"] = new SelectList(_context.Recipes, "RecipeId", "Instruction");
            return Page();
        }

        [BindProperty]
        public IngredientRecipe IngredientRecipe { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.IngredientRecipes.Add(IngredientRecipe);
            foreach (var recipe in _context.Recipes)
            {
                if (recipe.RecipeId != IngredientRecipe.RecipeId) continue;
                if (recipe.IngredientRecipes == null)
                {
                    recipe.IngredientRecipes = new List<IngredientRecipe>();
                }
                recipe.IngredientRecipes.Add(IngredientRecipe);
            }
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
