using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Recipes
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Recipe> Recipe { get;set; }

        public string Search { get; set; }
        public string ToContain { get; set; }
        public string NotToContain { get; set; }

        public int NumberOfPax { get; set; } = 1;

        public int MaxTimeToMake { get; set; }

        public string SortBy { get; set; }
        
        
        public async Task OnGetAsync(string search, string toDoActionReset, string toContain, string notToContain, 
            string sortBy, int numberOfPax, int maxTimeToMake)
        {

            if (toDoActionReset == "Reset")
            {
                Search = "";
                ToContain = "";
                NotToContain = "";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(search))
                { 
                    Search = search.ToLower().Trim();
                }
                if (!string.IsNullOrWhiteSpace(toContain))
                { 
                    ToContain = toContain.ToLower().Trim();
                }
                if (!string.IsNullOrWhiteSpace(notToContain))
                { 
                    NotToContain = notToContain.ToLower().Trim();
                }

                NumberOfPax = numberOfPax;

                MaxTimeToMake = maxTimeToMake;
                SortBy = sortBy;
            }

            var recipeQuery = _context.Recipes
                .Include(r => r.Category)
                .Include(r => r.IngredientRecipes)
                .ThenInclude(i => i.Ingredient)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(ToContain))
            {
                recipeQuery = recipeQuery.Where(r =>
                    r.IngredientRecipes.Any(i => i.Ingredient.IngredientName.ToLower() == ToContain));
            }
            
            if (!string.IsNullOrWhiteSpace(NotToContain))
            {
                recipeQuery = recipeQuery.Where(r =>
                    r.IngredientRecipes.All(i => i.Ingredient.IngredientName.ToLower() != NotToContain));
            }

            if (!string.IsNullOrWhiteSpace(Search))
            {
                recipeQuery = recipeQuery.Where(r =>
                    r.IngredientRecipes.Any(i => i.Ingredient.IngredientName.ToLower() == Search) ||
                    r.RecipeName.ToLower().Contains(Search));
            }
            
            
            if (NumberOfPax > 1)
            {
                foreach (var recipe in recipeQuery)
                {
                    foreach (var ingredientRecipe in recipe.IngredientRecipes)
                    {
                        ingredientRecipe.IngredientAmount = ingredientRecipe.IngredientAmount * NumberOfPax / recipe.Servings;
                    }
                }
            }

            if (NumberOfPax > 1)
            {
                recipeQuery = recipeQuery.Select(r => new Recipe
                {
                    Category = r.Category,
                    Instruction = r.Instruction,
                    Servings = NumberOfPax,
                    CategoryId = r.CategoryId,
                    IngredientRecipes = r.IngredientRecipes,
                    RecipeId = r.RecipeId,
                    RecipeName = r.RecipeName,
                    TimeNeeded = r.TimeNeeded * NumberOfPax / r.Servings
                });  
            }
            
            if (MaxTimeToMake > 0)
            {
                recipeQuery = recipeQuery.Where(r => r.TimeNeeded <= MaxTimeToMake);
            }
            
            if (SortBy == "TimeLow")
            {
                recipeQuery = recipeQuery.OrderBy(r => r.TimeNeeded);
            }

            if (SortBy == "TimeHigh")
            {
                recipeQuery = recipeQuery.OrderByDescending(r => r.TimeNeeded);
            }
            

            Recipe = await recipeQuery.ToListAsync();
        }
    }
}
