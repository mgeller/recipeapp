﻿using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Pages.Recipes
{
    public class FindRecipeModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public FindRecipeModel(DAL.AppDbContext context)
        {
            _context = context;
        }
        
        [BindProperty]
        public Ingredient Ingredient { get; set; }

        public string ToDoAction { get; set; }

        public List<Ingredient> ChosenIngredients { get; set; } = new List<Ingredient>();
        
        public ActionResult OnGet()
        {
            ViewData["LocationId"] = new SelectList(_context.Locations, "LocationId", "LocationName");
            return Page();
        }

        public ActionResult OnPost()
        {
            if (ToDoAction == "Insert")
            {
                ChosenIngredients.Add(Ingredient);
            }
            return Page();
        }
    }
}