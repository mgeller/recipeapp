﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {

        [Required] public DbSet<Ingredient> Ingredients { get; set; } = default!;
        [Required] public DbSet<IngredientRecipe> IngredientRecipes { get; set; } = default!;
        [Required] public DbSet<Recipe> Recipes { get; set; } = default!;
        [Required] public DbSet<Category> Categories { get; set; } = default!;
        [Required] public DbSet<Location> Locations { get; set; } = default!;
        
        public AppDbContext()
        {
            
        }
        
        public AppDbContext(DbContextOptions options): base(options)
        {
        }

    }
}